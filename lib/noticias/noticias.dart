import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:practica3/noticias/bloc/noticias_bloc.dart';
import 'package:practica3/noticias/noticia_deportes.dart';
import 'package:practica3/noticias/noticia_negocios.dart';

class Noticias extends StatefulWidget {
  Noticias({Key key}) : super(key: key);

  @override
  _NoticiasState createState() => _NoticiasState();
}

class _NoticiasState extends State<Noticias> {
  //final _noticiasContentList = [NoticiaDeportes(), NoticiaNegocios()];
  final _tabsList = [
    Tab(
      icon: Icon(
        Icons.sports_rounded,
      ),
      text: 'Deportes',
    ),
    Tab(
      icon: Icon(Icons.show_chart_rounded),
      text: 'Negocios',
    )
  ];
  @override
  Widget build(BuildContext context) {
    return Container(
      child: DefaultTabController(
        length: 2,
        child: Container(
          child: Scaffold(
            appBar: AppBar(
              title: Text('Material App'),
              bottom: TabBar(
                tabs: _tabsList,
              ),
            ),
            body: BlocProvider(
              create: (context) => NoticiasBloc()..add(GetNewsEvent()),
              child: BlocConsumer<NoticiasBloc, NoticiasState>(
                listener: (context, state) {
                  // TODO: Estado de error
                },
                builder: (context, state) {
                  if (state is NoticiasSuccessState) {
                    return TabBarView(
                      children: [
                        NoticiaDeportes(
                          noticias: state.noticiasSportList,
                        ),
                        NoticiaNegocios(
                          noticias: state.noticiasBusinessList,
                        ),
                      ],
                    );
                  }
                  return Center(
                    child: Text('Noticias no disponibles.'),
                  );
                },
              ),
            ),
          ),
        ),
      ),
    );
  }
}
