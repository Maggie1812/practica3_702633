import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart';
import 'package:practica3/models/noticias.dart';
import 'package:practica3/secrets.dart';

part 'noticias_event.dart';
part 'noticias_state.dart';

class NoticiasBloc extends Bloc<NoticiasEvent, NoticiasState> {
  final _sportsLink =
      "http://newsapi.org/v2/top-headlines?country=mx&category=sports&$apiKey";

  final _businessLink =
      "http://newsapi.org/v2/top-headlines?country=mx&category=business&$apiKey";

  NoticiasBloc() : super(NoticiasInitial());

  @override
  Stream<NoticiasState> mapEventToState(
    NoticiasEvent event,
  ) async* {
    if (event is GetNewsEvent) {
      // Request and de-serialize JSON to use as Dart Object
      // Map result to a List of 'Noticias', yield to 'NoticiasInitial'
      try {
        List<Noticia> sportsNews = await _requestSportNews();
        List<Noticia> businessNews = await _requestBusinessNews();
        yield NoticiasSuccessState(
            noticiasBusinessList: businessNews, noticiasSportList: sportsNews);
      } catch (e) {
        yield NoticiasErrorState(error: 'Error al intentar obtener noticias');
      }
    }
  }

  Future<List<Noticia>> _requestSportNews() async {
    Response response = await get(_sportsLink);
    List<Noticia> _noticiasList = List();
    if (response.statusCode == 200) {
      List<dynamic> data = jsonDecode(response.body)['articles'];
      _noticiasList = (data.map((e) => Noticia.fromJson(e))).toList();
    }
    return _noticiasList;
  }

  Future<List<Noticia>> _requestBusinessNews() async {
    Response response = await get(_businessLink);
    List<Noticia> _noticiasList = List();
    if (response.statusCode == 200) {
      List<dynamic> data = jsonDecode(response.body)['articles'];
      _noticiasList = (data.map((e) => Noticia.fromJson(e))).toList();
    }
    return _noticiasList;
  }
}
