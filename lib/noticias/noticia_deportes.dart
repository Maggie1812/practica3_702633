import 'package:flutter/material.dart';
import 'package:practica3/models/noticias.dart';
import 'package:practica3/noticias/item_noticia.dart';

class NoticiaDeportes extends StatefulWidget {
  final List<Noticia> noticias;
  NoticiaDeportes({Key key, @required this.noticias}) : super(key: key);

  @override
  NoticiaDeportesState createState() => NoticiaDeportesState();
}

class NoticiaDeportesState extends State<NoticiaDeportes> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
        itemCount: widget.noticias.length,
        itemBuilder: (BuildContext context, int index) {
          return ItemNoticia(noticias: widget.noticias[index]);
        },
      ),
    );
  }
}
