import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:practica3/mis_noticias/bloc/mis_noticias_bloc.dart';

class CrearNoticia extends StatefulWidget {
  CrearNoticia({Key key}) : super(key: key);

  @override
  _CrearNoticiaState createState() => _CrearNoticiaState();
}

class _CrearNoticiaState extends State<CrearNoticia> {
  MisNoticiasBloc _misNoticiasBloc;
  String _titulo;
  String _descripcion;
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) {
        _misNoticiasBloc = MisNoticiasBloc();
        return _misNoticiasBloc;
      },
      child: BlocConsumer<MisNoticiasBloc, MisNoticiasState>(
        listener: (context, state) {},
        builder: (context, state) {
          return Scaffold(
            body: Center(),
            floatingActionButton: FloatingActionButton(
              backgroundColor: Colors.blue,
              child: Icon(Icons.more),
              onPressed: () {
                _showNewNewsDialog();
              },
            ),
          );
        },
      ),
    );
  }

  Future<void> _showNewNewsDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Nueva Noticia'),
          content: SingleChildScrollView(
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  TextFormField(
                    decoration: InputDecoration(hintText: "Titulo"),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Por favor ingresa un titulo';
                      }
                      _titulo = value;
                      return null;
                    },
                  ),
                  TextFormField(
                    decoration: InputDecoration(hintText: "Descripcion"),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Por favor ingresa la descripcion';
                      }
                      _descripcion = value;
                      return null;
                    },
                  ),
                  SizedBox(height: 15),
                  IconButton(
                    icon: Icon(Icons.add_a_photo),
                    onPressed: () {
                      _misNoticiasBloc
                          .add(CargarImagenEvent(takePictureFromCamera: true));
                    },
                  )
                ],
              ),
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Crear'),
              onPressed: () {
                if (_formKey.currentState.validate()) {
                  _misNoticiasBloc.add(CrearNoticiaEvent(
                      titulo: _titulo,
                      descripcion: _descripcion,
                      autor: 'Maggie',
                      fuente: 'Maggie'));
                  Navigator.of(context).pop();
                }
              },
            ),
          ],
        );
      },
    );
  }
}
