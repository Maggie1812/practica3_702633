import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:practica3/mis_noticias/bloc/mis_noticias_bloc.dart';
import 'package:practica3/noticias/item_noticia.dart';

class MisNoticias extends StatelessWidget {
  MisNoticiasBloc _misNoticiasBloc;
  MisNoticias({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider(
        create: (context) {
          this._misNoticiasBloc = MisNoticiasBloc();
          return this._misNoticiasBloc..add(LeerNoticiasEvent());
        },
        child: BlocConsumer<MisNoticiasBloc, MisNoticiasState>(
          listener: (context, state) {},
          builder: (context, state) {
            if (state is NoticiasDescargadasState) {
              return ListView.builder(
                itemCount: _misNoticiasBloc.getNewsList.length,
                itemBuilder: (BuildContext context, int index) {
                  return ItemNoticia(
                      noticias: _misNoticiasBloc.getNewsList[index]);
                },
              );
            } else
              return Center(child: Text('No hay noticias disponibles'));
          },
        ),
      ),
    );
  }
}
